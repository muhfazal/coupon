<?php

namespace App\Http\Livewire\Product;

use App\Models\Product;
use App\Models\ProductImages;
use App\Models\Shop;
use App\Models\User;
use App\Utils\CommonUtils;
use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithFileUploads;
use Storage;

class Create extends Component
{
    use WithFileUploads;

    public $productName;
    public $description;
    public $pointsNeeded;
    public $isEnabled = true;
    public $images = [];

    protected $rules = [
        'productName' => 'required',
        'pointsNeeded' => 'required',
        'images.*' => 'image',
    ];

    public function submit() {
        $this->validate();
        $product = new Product();
        $product->name = $this->productName;
        $product->description = $this->description;
        $product->points_needed = $this->pointsNeeded;
        $product->is_active = $this->isEnabled;
        $product->save();
        foreach($this->images as $image) {
            $path = $image->store('products', 's3');
            Storage::disk('s3')->setVisibility($path, 'public');
            $i = new ProductImages();
            $i->product_id = $product->id;
            $i->image = $path;
            $i->save();
        }

        // $user = new User();
        // $user->name = $this->shopName;
        // $user->email = $this->email;
        // $user->phone = $this->phone;
        // $user->password =  bcrypt($this->password);
        // $user->menuroles = ('user,shop');
        // $user->save();
        // $user->assignRole('user');
        // $user->assignRole('shop');
        // $shop = new Shop();
        // $shop->name = $this->shopName;
        // $shop->address = $this->address;
        // $shop->available_coupons = CommonUtils::getSettings('initial_coupons_for_shop');
        // $shop->user_id = $user->id;
        // $shop->save();
        session()->flash('message', 'Product added Successfully');
        $this->productName = $this->description = $this->pointsNeeded = '';
        $this->isEnabled = true;
        $this->images= [];

    }
    public function render()
    {
        return view('livewire.product.create')
        ->extends('dashboard.base');
    }
}
