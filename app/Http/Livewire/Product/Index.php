<?php

namespace App\Http\Livewire\Product;

use App\Models\Notes;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\Shop;
use Livewire\Component;

class Index extends Component
{
    public $header = 'Shops';

    public function delete($id) {
        ProductImages::where('product_id', $id)->delete();
        Product::destroy($id);
        session()->flash('message', 'Product deleted Successfully');
    }

    public function redirectToEdit($id) {
        redirect()->route('backend.product.edit',$id);
    }

    public function render()
    {
        return view('livewire.product.index', [
            'products' => Product::with('images')->paginate(config('constants.pagination_count')),
        ])

        ->extends('dashboard.base');
    }
}
