<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Utils\CommonUtils;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ShopLogin extends Component
{
    /** @var string */
    public $phone = '';

    public $isOtpSent = false;

    public $otp = '';

    protected $rules = [
        'phone' => ['required'],
        'otp' => ['required'],
    ];

    public function sendOtp() {
        if($this->phone) {
            // check user exists
            $user = User::where('phone', $this->phone)->first();
            // ## to do send otp
            if($user) {
                $this->otp = CommonUtils::generateOtp();
                $this->isOtpSent = true;
            }
            else {
                session()->flash('error', 'This mobile number is not registered with us yet');
            }
        } else {
            session()->flash('error', 'Please enter your mobile number');
        }
    }

    public function authenticate()
    {
        $this->validate();
        $user = User::where('phone', $this->phone)->first();
        Auth::login($user);
        if(CommonUtils::isAdmin() === true || CommonUtils::isShop() === true) {
            return redirect()->route('backend.home');
        } else {
            return  redirect()->route('site.home');
        }
    }

    public function render()
    {
        return view('livewire.auth.shop-login')->extends('layouts.auth');

    }
}
