<?php

namespace App\Http\Livewire\Site;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Utils\CommonUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Component;

class Login extends Component
{
    /** @var string */
    public $phone = '';

    /** @var string */
    public $otp = '';

    public $isOtpSent = false;

    /** @var bool */
    // public $remember = false;

    protected $rules = [
        'phone' => ['required'],
        'otp' => ['required'],
    ];


    public function authenticate(Request $request)
    {
        $this->validate();

        // $this->validate();
        $user = User::where('phone', $this->phone)->first();
        Auth::login($user);
        return redirect()->intended(route('site.login'));



    }

    public function sendOtp() {
        $this->validate(['phone' => 'required|exists:users,phone'
        ]);
        // check user exists
        $user = User::where('phone', $this->phone)->first();

        // ## to do send otp
        if($user) {
            $this->otp = CommonUtils::generateOtp();
            $this->isOtpSent = true;
        }
        else {
            session()->flash('phone', 'This mobile number is not registered with us yet');
        }
    }





    public function render()
    {
        return view('site.pages.login')->extends('site.non-auth-layout');

    }
}
