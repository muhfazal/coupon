<?php

namespace App\Http\Livewire\Site;

use App\Models\User;
use Livewire\Component;
use Auth;

class Home extends Component
{

    public $user;
    public function mount()
    {
        $this->user = User::with('coupons')->find(Auth::user()->id);
    }

    public function render()
    {
        return view('livewire.dashboard')
        ->extends('site.layout');
    }
}
