<?php

namespace App\Http\Livewire\Shop;

use App\Models\Shop;
use App\Models\User;
use App\Utils\CommonUtils;
use Illuminate\Http\Request;
use Livewire\Component;

class Edit extends Component
{
    public $shopName;
    public $address;
    public $email;
    public $phone;
    public $password;
    public $password_confirmation;
    public $shop;
    public $coupons;
    public $points_coupons;

    protected $rules = [
        'shopName' => 'required',
        'address' => 'required',
        'phone' => 'required',
        'password' => 'confirmed',
        'coupons'=> 'required',
        'points_coupons'=>'required'
    ];

    public function mount(Request $request)
    {
        $this->shop = Shop::with('user')->find($request->id);
        $this->fill(['shopName' => $this->shop->name, 'address'=> $this->shop->address,
        'phone' => $this->shop->user->phone, 'email' => $this->shop->user->email,
        'coupons'=>$this->shop->available_coupons]);
    }

    public function submit() {
        $this->validate();
        $user = User::find($this->shop->user_id);
        $user->name = $this->shopName;
        $user->email = $this->email;
        $user->phone = $this->phone;
        if($this->password) {
            $user->password =  bcrypt($this->password);
        }
        $user->menuroles = ('user,shop');
        $user->save();

        $shop = Shop::find($this->shop->id);
        $shop->name = $this->shopName;
        $shop->address = $this->address;
        $shop->available_coupons = $this->coupons;
        $shop->points_coupons = $this->points_coupons;
        $shop->save();
        session()->flash('message', 'Shop updated Successfully');
        redirect()->route('backend.shops');

    }
    public function render()
    {
        return view('livewire.shop.edit')
        ->extends('dashboard.base');
    }
}
