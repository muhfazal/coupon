<?php

namespace App\Http\Livewire\Shop;

use App\Models\Notes;
use App\Models\Shop;
use Livewire\Component;

class Index extends Component
{
    public $header = 'Shops';

    public function delete($id) {
        Shop::destroy($id);
        session()->flash('message', 'Shop deleted Successfully');
    }

    public function redirectToEdit($id) {
        redirect()->route('backend.shops.edit',$id);
    }

    public function render()
    {
        return view('livewire.shop.index', [
            'shops' => Shop::with('user')->paginate(config('constants.pagination_count')),
        ])

        ->extends('dashboard.base');
    }
}
