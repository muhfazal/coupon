<?php

namespace App\Http\Livewire\Shop;

use App\Models\Shop;
use App\Models\User;
use App\Utils\CommonUtils;
use Livewire\Component;

class Create extends Component
{
    public $shopName;
    public $address;
    public $email;
    public $phone;
    public $password;
    public $coupons;
    public $password_confirmation;
    public $points_coupons;

    protected $rules = [
        'shopName' => 'required',
        'address' => 'required',
        'email'  =>'required',
        'phone' => 'required|unique:users',
        'password' => 'confirmed',
        'coupons'=> 'required',
        'points_coupons'=>'required'
    ];

    public function submit() {
        //dd($this->no_coupons);
        $this->validate();
        $user = new User();
        $user->name = $this->shopName;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->password =  bcrypt($this->password);
        $user->menuroles = ('user,shop');
        $user->save();
        $user->assignRole('user');
        $user->assignRole('shop');
        $shop = new Shop();
        $shop->name = $this->shopName;
        $shop->address = $this->address;
        $shop->available_coupons = $this->coupons;
        $shop->user_id = $user->id;
        $shop->points_coupons =$this->points_coupons;
        $shop->save();
        session()->flash('message', 'Shop created Successfully');
        $this->shopName = $this->address = $this->email = $this->phone = $this->password = $this->password_confirmation = '';

    }
    public function render()
    {
        return view('livewire.shop.create')
        ->extends('dashboard.base');
    }
}
