<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Product;
use App\Models\ProductCart;
use App\Models\UserPoints;
use App\Models\UserAddress;
use App\Models\CouponIssues;
use App\Models\UserProduct;
use Livewire\Component;
use Illuminate\Http\Request;
use Auth;

class ViewCart extends Component
{
  public $header = 'Products';
  public $user;
  public $cart;
  public $msg;
  public $product;
  public $address;
   public $userproduct;

  public $cartsum;
  public $points;
  public $points_data;
  public $finalpoints;
   public $username;
   public $firstaddress;
   public $lastaddress;
   public $postcode;
   public $landmark;
   public $result;

  public function addaddress(){
    // dd($this->address);
     // dd($this->firstaddress);
    $this->user=User::where('id',Auth::user()->id)->first();
    $this->user->delivery_address=$this->address;
    $this->user->save();
    // $this->result = UserAddress::create([
    //                              'user_id'=> Auth::user()->id,
    //                              'username'=> $this->username,
    //                              'first_address'=> $this->firstaddress,
    //                               'second_address'=>$this->lastaddress,
    //                               'postcode' =>$this->lastaddress,
    //                               'landmark' =>$this->landmark
    //                               ]);

     

    //************************
       $this->cartsum=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->join('products','products.id','product_cart.product_id')->sum('points_needed');

       $this->points = UserPoints::where('user_id',Auth::user()->id)->first();
       
       if($this->points->total_points>=$this->cartsum){
        $this->cartitems=ProductCart::where('user_id',Auth::user()->id)->where('status', 1)->update(['status'=>2,'username'=> $this->username,
                                 'first_address'=> $this->firstaddress,
                                  'last_address'=>$this->lastaddress,
                                  'postcode' =>$this->postcode,
                                  'landmark' =>$this->landmark]);
        $this->finalpoints = $this->points->total_points - $this->cartsum ;
        //dd($this->finalpoints );
        $this->points->total_points = $this->finalpoints;
        $this->points->save();
        $this->msg='Product purchased';

       }
       else{
        $this->msg='Not enough points to purchase';
       }
    //************************
  }
  public function render(Request $request)
  {
       // dd('hit');
       $this->user=User::where('id',Auth::user()->id)->first();
       $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)
                   ->join('products','products.id','product_cart.product_id')
                  ->get();
                  // dd($this->cart);
       



       return view('livewire.customer.cart')->extends('site.layout');
  }

}







 