<?php

namespace App\Http\Livewire\User;

use App\Models\Notes;
use App\Models\User;
use App\Models\Shop;
use App\Models\CouponUser;
use Livewire\Component;

class ListUser extends Component
{
    public $header = 'Shops';

   
    Public $coupon;


    public function render()
    {
         
     
         return view('livewire.users.listuser', [
            'lists' => User::where('menuroles','user')->paginate(10),
        ])

        ->extends('dashboard.base');
        //
    }
}
