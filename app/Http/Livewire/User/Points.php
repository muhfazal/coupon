<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Customer;
use App\Models\UserPoints;
use App\Models\CouponIssues;
use Livewire\Component;
use Auth;

class Points extends Component
{

  public $user;
  public $points;

   public function mount()
   {
     $this->user=User::where('id',Auth::user()->id)->first();
  
     $this->points = UserPoints::where('user_id',Auth::user()->id)->first();
     // dd($this->points);
   }
    




    public function render()
    {
    	//$this->customer=Customer::get();
        return view('livewire.customer.points')->extends('site.layout');
        
    }
}
