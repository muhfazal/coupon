<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Product;
use App\Models\ProductCart;
use App\Models\UserPoints;
use App\Models\CouponIssues;
use Livewire\Component;
use Illuminate\Http\Request;
use Auth;

class RemoveCart extends Component
{
 
  public $cartsum;
  public $points;
  public $points_data;
  public $msg;
  public $cart;
  public $user;
  public $finalpoints;
  public function mount(Request $request)
  {

       $this->cartsum=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->join('products','products.id','product_cart.product_id')->sum('points_needed');

       $this->points = UserPoints::where('user_id',Auth::user()->id)->first();
       
       if($this->points->total_points>=$this->cartsum){
        $this->cartitems=ProductCart::where('user_id',Auth::user()->id)->update(['status'=>2]);
        $this->finalpoints = $this->points->total_points - $this->cartsum ;
        //dd($this->finalpoints );
        $this->points->total_points = $this->finalpoints;
        $this->points->save();
        $this->msg='Product purchased';

       }
       else{
        $this->msg='Not enough points to purchase';
       }
       //  ProductCart::create(['user_id'=>Auth::user()->id,
       //                  'product_id'=>$request->segment(4),
       //                  'status'=>1
       //                  ]);
       // $this->user=User::where('id',Auth::user()->id)->first();
       // $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->count();
       // return redirect('/dashboard/products');
     // $this->product= Product::Where($id);
        
  }
  public function render(Request $request)
  {
       $this->user=User::where('id',Auth::user()->id)->first();
       $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->get();
       return view('livewire.customer.cart')->extends('site.layout');
  }

}







 