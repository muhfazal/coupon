<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Shop;
use App\Models\Customer;
use App\Models\CouponUser;
use App\Models\CouponIssues;
use Livewire\Component;
use Auth;

class HomeLogin extends Component
{

  //public $header = 'Customer';
    public $user;
    Public $coupon;
    Public $shop;
    Public $win;
    Public $points;
    public function mount()
    {
        $this->user = User::with('coupons')->find(Auth::user()->id);
    }

    public function render()
    {

     $this->win = User::where('users.id' , Auth::user()->id)
                        ->join('coupon_user' , 'coupon_user.user_id', 'users.id')
                        ->join('coupons','coupons.id', 'coupon_user.coupon_id')
                        ->where('is_disposable', 2 )
                        ->latest('coupons.id' )
                        ->first();

     //dd($this->win);  

  


        // $this->shop=Shop::join('coupon_issues', 'coupon_issues.shop_id' ,'shops.id' )
        //             ->join('coupons','coupons.id','coupon_user.coupon_id')
        //             ->groupBy('coupon_issues.shop_id')->select('shops.id','name')
        //             ->where('coupon_issues.user_id', Auth::user()->id )
        //             ->get();
       // dd($this->shop);
        $this->shop=Shop::join('issued_coupons', 'issued_coupons.shop_id' ,'shops.id' )
                    ->join('coupon_user','coupon_user.coupon_id','issued_coupons.coupon_id')
                    ->join('coupons', 'coupons.id' , 'coupon_user.coupon_id')
                    ->groupBy('issued_coupons.shop_id')->select('shops.id','name')
                    ->where('coupon_user.user_id', Auth::user()->id )
                    ->where('is_disposable' , 1)
                    ->get();


  // dd($this->shop);
    	$this->coupon=CouponUser::Where('user_id',Auth::user()->id)
        ->join('coupons','coupons.id','coupon_user.coupon_id')
        ->join('issued_coupons','issued_coupons.coupon_id','coupon_user.coupon_id')
        ->where('is_disposable', 1 )
        ->get();

     

// dd($this->points);
       
        return view('site.login_dashboard')->extends('site.layout');
        
    }
}
