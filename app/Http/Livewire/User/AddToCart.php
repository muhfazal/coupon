<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Product;
use App\Models\ProductCart;
use Livewire\Component;
use Illuminate\Http\Request;
use Auth;

class AddToCart extends Component
{
  public $header = 'Products';
  public $user;
  public $cart;

  public function mount(Request $request)
  {
        ProductCart::create(['user_id'=>Auth::user()->id,
                        'product_id'=>$request->segment(4),
                        'status'=>1
                        ]);
       $this->user=User::where('id',Auth::user()->id)->first();
       $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->count();
       return redirect('/dashboard/products');
     // $this->product= Product::Where($id);
        
  }

  public function render(Request $request)
  {
       // ProductCart::create(['user_id'=>Auth::user()->id,
       //                  'product_id'=>$request->segment(4),
       //                  'status'=>1
       //                  ]);
       // $this->user=User::where('id',Auth::user()->id)->first();
       // $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->count();
       // return redirect('/dashboard/products');

       // return view('livewire.customer.products', [
       //      'products' => Product::with('images')->paginate(10),
       //  ])->extends('site.layout');
  }

}







 