<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Product;
use Livewire\Component;
use Auth;

class ListProducts extends Component
{

  public $header = 'Products';

  public function render(Request $request)
    {
        $this->product = Product::with('user')->find($request->id);
        return view('livewire.customer.listproduct')->extends('site.layout');
    }


   
}


