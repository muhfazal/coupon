<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Product;
use App\Models\ProductCart;
use Livewire\Component;
use Illuminate\Http\Request;
use Auth;

class RemoveFromCart extends Component
{
  public $header = 'Products';
  public $user;
  public $cart;
  public $cart1;
  public $msg;
  public function render(Request $request)
  { //dd($request->segment(4));
       $this->user=User::where('id',Auth::user()->id)->first();
       $this->cart1=ProductCart::where('product_id',$request->segment(4))->delete();
       $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)
      ->join('products','products.id','product_cart.product_id')
       ->get();
       return view('livewire.customer.cart')->extends('site.layout');
  }

}







 