<?php

namespace App\Http\Livewire\User;

use App\Models\Notes;
use App\Models\User;
use App\Models\CouponUser;
use App\Models\Shop;
use Livewire\Component;
use Illuminate\Http\Request;
class UserDetails extends Component
{
    public $header = 'Shops';

   Public $users;
   Public $shop;
   Public $coupon;
   Public $expshop;
   Public $expcoupon;
   Public $couponcount;
   Public $expcouponcount;  

    public function render(Request $request)
    {

      $user_id=$request->segment(4);
    //dd($user_id);

     $this->shop=Shop::join('issued_coupons', 'issued_coupons.shop_id' ,'shops.id' )
                    ->join('coupon_user','coupon_user.coupon_id','issued_coupons.coupon_id')
                    ->join('coupons', 'coupons.id' , 'coupon_user.coupon_id')
                    ->groupBy('issued_coupons.shop_id')->select('shops.id','name')
                    ->where('coupon_user.user_id', $user_id )
                    ->where('is_disposable' , 1)
                    ->get();


    $this->coupon=CouponUser::Where('user_id',$user_id )
                 ->join('coupons','coupons.id','coupon_user.coupon_id')
                 ->join('issued_coupons','issued_coupons.coupon_id','coupon_user.coupon_id')
                 ->where('is_disposable', 1 )
                 ->get();          
      $this->couponcount=count($this->coupon);

  //$this->customer=Customer::get();
     $this->expshop=Shop::join('issued_coupons', 'issued_coupons.shop_id' ,'shops.id' )
                    ->join('coupon_user','coupon_user.coupon_id','issued_coupons.coupon_id')
                    ->join('coupons', 'coupons.id' , 'coupon_user.coupon_id')
                    ->groupBy('issued_coupons.shop_id')->select('shops.id','name')
                    ->where('coupon_user.user_id', $user_id)
                    ->where('is_disposable' , 0)
                    ->get();


  // dd($this->shop);
      $this->expcoupon=CouponUser::Where('user_id',$user_id)
                       ->join('coupons','coupons.id','coupon_user.coupon_id')
                       ->join('issued_coupons','issued_coupons.coupon_id','coupon_user.coupon_id')
                       ->where('is_disposable', 0 )
                       ->get();

         $this->expcouponcount=count($this->expcoupon);



        return view('livewire.users.userdetails')

        ->extends('dashboard.base');
    }
}
