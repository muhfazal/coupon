<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Shop;
use App\Models\CouponUser;
use App\Models\Customer;
use Livewire\Component;
use Auth;

class Coupens extends Component
{

  //public $header = 'Customer';

Public $shop;
Public $coupon;
    public function render()
    {
    	//$this->customer=Customer::get();
     $this->shop=Shop::join('issued_coupons', 'issued_coupons.shop_id' ,'shops.id' )
                    ->join('coupon_user','coupon_user.coupon_id','issued_coupons.coupon_id')
                    ->join('coupons', 'coupons.id' , 'coupon_user.coupon_id')
                    ->groupBy('issued_coupons.shop_id')->select('shops.id','name')
                    ->where('coupon_user.user_id', Auth::user()->id )
                    ->where('is_disposable' , 0)
                    ->get();


  // dd($this->shop);
    	$this->coupon=CouponUser::Where('user_id',Auth::user()->id)
        ->join('coupons','coupons.id','coupon_user.coupon_id')
        ->join('issued_coupons','issued_coupons.coupon_id','coupon_user.coupon_id')
        ->where('is_disposable', 0 )
        ->get();



        return view('livewire.customer.coupens')->extends('site.layout');
        
    }
}
