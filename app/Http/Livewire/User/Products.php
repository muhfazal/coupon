<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Models\Product;
use App\Models\ProductCart;
use App\Models\UserPoints;
use Livewire\Component;
use Auth;

class Products extends Component
{

  public $header = 'Products';
  public $user;
  public $cart;
  public $userpoints;


  public function mount()
  {
       
        //$this->product = Product::with('user')->find());
        return view('livewire.customer.listproducts')->extends('site.layout');
  }

  public function productlist($id) {
         //dd($id);
        redirect()->route('customer.product.list',$id);
         //return view('livewire.customer.products')->extends('site.layout');
  }

  public function render()
  { 
        $this->user=User::where('id',Auth::user()->id)->first();
        $this->cart=ProductCart::where('user_id',Auth::user()->id)->where('status',1)->count();
        // dd($this->user);
        $this->userpoints = UserPoints::where('user_id',Auth::user()->id)->first();
        //dd($this->userpoints );
        return view('livewire.customer.products', [
            'products' => Product::with('images')->orderBy('points_needed', 'asc')->paginate(10),
        ])->extends('site.layout');
  }
}







 