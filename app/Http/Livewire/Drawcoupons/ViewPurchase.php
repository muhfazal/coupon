<?php

namespace App\Http\Livewire\Drawcoupons;

use App\Models\User;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\CouponUser;
use App\Models\ProductCart;
use App\Models\UserAddress;
use Livewire\Component;
use Auth;

use Illuminate\Http\Request;
class ViewPurchase extends Component
{
 
 Public $userpurchases;
  
   
    public function render()
     {

    $this->userpurchases=ProductCart::where('status',2)->join('products','products.id','product_cart.product_id')->get();
                                      
                                   

//dd($this->userpurchases);


        return view('livewire.drawcoupon.viewpurchase')
        ->extends('dashboard.base');
    }     
    

   
}
