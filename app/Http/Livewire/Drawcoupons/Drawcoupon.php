<?php

namespace App\Http\Livewire\Drawcoupons;

use App\Models\User;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\CouponUser;
use Livewire\Component;
use Auth;

use Illuminate\Http\Request;
class Drawcoupon extends Component
{
 
  //public $header = 'Customer';
  public $drawcoupn;
    Public $coupon;
     Public $coupon_date;

    public function drawcoupon(Request $request)
    {
      //dd($this->coupon_date);
         $this->coupon = Coupon::inRandomOrder()
                         ->join('issued_coupons','issued_coupons.coupon_id','coupons.id')
                         ->whereDate('issued_coupons.created_at','<=',$this->coupon_date)
                         ->where('is_disposable', 1 )
                         ->join('coupon_user','coupon_user.coupon_id', 'coupons.id')
                         ->join('users','users.id', 'coupon_user.user_id')
                         ->select('coupons.*' ,'users.name','users.phone')
                         ->first();

        //dd($this->coupon);
         // $draw =Coupon::find($this->coupon->id);
        if($this->coupon ){
         $this->coupon->is_disposable = 2;
         $this->coupon->save();
         $draw_status=Coupon::where('coupons.id','!=',$this->coupon->id )->
         join('issued_coupons','issued_coupons.coupon_id','coupons.id')->whereDate('issued_coupons.created_at','<=',$this->coupon_date)->where('is_disposable', 1 )->update(['is_disposable'=>0]);

         $win_status=Coupon::where('coupons.id','!=',$this->coupon->id )->
         join('issued_coupons','issued_coupons.coupon_id','coupons.id')->whereDate('issued_coupons.created_at','<=',$this->coupon_date)->where('is_disposable', 2 )->update(['is_disposable'=>3]);

       }
        
          return view('livewire.drawcoupon.drawcoupon')
        ->extends('dashboard.base');
    }

    public function render()
     {

        return view('livewire.drawcoupon.drawcoupon')
        ->extends('dashboard.base');
    }
}
