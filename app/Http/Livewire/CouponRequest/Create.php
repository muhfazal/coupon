<?php

namespace App\Http\Livewire\CouponRequest;

use App\Models\CouponRequest;
use App\Utils\CommonUtils;
use Livewire\Component;

class Create extends Component
{
    public $couponSet;
    public $numberOfSet;
    public function mount() {
        
        //$this->couponSet = CommonUtils::getSettings('coupons_in_set');
        $this->numberOfSet = 0;
    }

    protected $rules = [  
        'numberOfSet' => 'required',
    ];
 
    public function submit() {
        //dd($this->numberOfSet);
        $this->validate();
        $cr = new CouponRequest();
        $cr->shop_id = CommonUtils::getShop()->id;
        $cr->no_of_sets = $this->numberOfSet;
        $cr->coupons_in_set = $this->couponSet;
        $cr->coupons = $this->numberOfSet;
        $cr->status = config('constants.request_status.pending');
        $cr->save();
        session()->flash('message', 'Request submitted Successfully');
        $this->numberOfSet = '';

    }
    public function render()
    {
        return view('livewire.requests.create',[
            'shops' => CouponRequest::with('user')->paginate(config('constants.pagination_count')),
        ])
        ->extends('dashboard.base');
    }
}
