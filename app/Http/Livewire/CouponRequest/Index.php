<?php

namespace App\Http\Livewire\CouponRequest;

use App\Models\CouponRequest;
use App\Models\Shop;
use App\Utils\CommonUtils;
use Livewire\Component;

class Index extends Component
{

    public $header = 'Coupon Requests';

    // public function delete($id) {
    //     Shop::destroy($id);
    //     session()->flash('message', 'Shop deleted Successfully');
    // }

    public function redirectToEdit($id) {
        redirect()->route('backend.shops.edit',$id);
    }

    public function acceptRequest($id) {
        $request = CouponRequest::find($id);
        $request->status = config('constants.request_status.fullfilled');
        $request->save();
        $shop = Shop::find($request->shop_id);
        $shop->available_coupons = $shop->available_coupons + $request->coupons;
        $shop->save();
        session()->flash('message', 'Request fullfilled Successfully');
    }

    public function render()
    {
        $requests = [];
        if(CommonUtils::isShop()) {
            $shop = CommonUtils::getShop();
            $requests = CouponRequest::with('user')->where('shop_id', $shop->id)->paginate(config('constants.pagination_count'));
        } else if (CommonUtils::isAdmin()) {
            $requests = CouponRequest::with('user')->paginate(config('constants.pagination_count'));
        }

        return view('livewire.requests.index',[
            'requests' => $requests
        ])
        ->extends('dashboard.base');
    }
}
