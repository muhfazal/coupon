<?php

namespace App\Http\Livewire\CouponIssues;

use App\Models\CouponIssues;
use App\Models\User;
use App\Utils\CommonUtils;
use App\Utils\CouponUtils;
use Livewire\Component;
use Auth;
use Request;

class Issue extends Component
{
    public $couponSet;
    public $numberOfCoupons;
    public $availableCoupons;
    public $phone;
    public $shopid;
    public $userdetails;
    public $userName ;

    public function mount() {
        $this->couponSet = CommonUtils::getSettings('coupons_in_set');
        $this->numberOfCoupons = 0;
        //dd('hit');
        $this->availableCoupons = CommonUtils::getShop()->available_coupons;
        // $this->availableCoupons = CommonUtils::getShop();
        // dd($this->availableCoupons);
        $this->phone = '';
        $this->userName ='';
    }

    public function issue() {

       // dd('hit');
        $this->validate([
            'phone' => ['required','min:10'],
            'numberOfCoupons' => ['numeric',"max:$this->availableCoupons"],
        ]);

        if(CouponUtils::issueCoupnToPhone($this->phone,$this->userName ,$this->numberOfCoupons, CommonUtils::getShop()->id)) {
            
            $this->availableCoupons = CommonUtils::getShop()->available_coupons;
            session()->flash('message', 'Coupon issued Successfully');
            $this->phone = $this->userName=$this->numberOfCoupons = '';
        } else {
            session()->flash('error', 'Coupon issue failed');
        }
            // create number of coupons  //coupons
            // create new coupon issue  //coupon issue
            // assign all coupons to user //coupons user
            // assign all coupons to coupon issue relation // issued coupons



    }
    public function username(Request $request)
    {
     


        $this->userdetails =User::where('phone',$this->phone)->first();
        if($this->userdetails&&$this->userdetails->name){

        $this->userName=$this->userdetails->name;
      
     }
     else
     {
       $this->userName=''; 
     }
    }



    public function render()
    {
        
          // $this->shopid =Auth::user()->id;
          // dd( $this->shopid);
        return view('livewire.coupon-issues.issue')->extends('dashboard.base');
    }
}
