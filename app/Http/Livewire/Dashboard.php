<?php

namespace App\Http\Livewire;

use App\Models\Shop;
use App\Utils\CommonUtils;
use Livewire\Component;

class Dashboard extends Component
{
    public function render()
    {

        return view('livewire.dashboard',[
            'shop'=> CommonUtils::getShop()
        ])
        ->extends('dashboard.base');
    }
}
