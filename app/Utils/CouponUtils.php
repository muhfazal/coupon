<?php
namespace App\Utils;

use App\Models\CouponIssues;
use App\Models\IssuedCoupons;
use App\Models\Shop;
use App\Models\User;
use App\Models\UserPoints;
use App\Utils\CommonUtils;
use Gabievi\Promocodes\Models\Promocode;
use Gabievi\Promocodes\Promocodes;

class Couponutils {
    public static function issueCoupnToPhone($phone, $userName,$numberOfCoupns, $shopId) {
        // create user if not exist  //users
        $user = User::where('phone',$phone)->first();
        if($user) {
            // add user role to him
            $user->assignRole('user');
        } else {
            $user = new User();
            $user->name = $userName;
            $user->phone = $phone;
            $user->password =  '';
            $user->menuroles = ('user');
            $user->save();
            $user->assignRole('user');

        }

         $shop_id=CommonUtils::getShop()->id;
         $points=Shop::where('id',$shop_id)->first();
         $user_points=$points->points_coupons;

        $coupnIssue = new CouponIssues();
        $coupnIssue->shop_id = $shop_id;
        $coupnIssue->user_id = $user->id;
        $coupnIssue->no_of_coupons = $numberOfCoupns;
        $coupnIssue->points = $user_points * $numberOfCoupns;


        $coupnIssue->reference = 'REF';

        $coupnIssue->save();
        //total points
        $totalpoints = UserPoints::where('user_id' ,$user->id)->first();
        if($totalpoints)
        {
          $totalpoints->total_points +=  $coupnIssue->points;
          $totalpoints->save();  
        }
        else{
            $totalpoints = UserPoints::create([
            'user_id' => $user->id,
            'total_points' =>$coupnIssue->points

            ]);

        }


        //

        $p = new Promocodes();
        $coupons = $p->createDisposable($numberOfCoupns,null,['issue_id'=>$coupnIssue->id]);
        // dd($coupons);
        $d = array();
        $shop_id = CommonUtils::getShop()->id;
        // dd($shop_id);
        foreach($coupons as $c) {
            $user->redeemCode($c['code']);
            $coupon = Promocode::where('code',$c['code'] )->first();
            $d[] = array('coupon_issue_id'=> $coupnIssue->id, 'coupon_id' => $coupon->id, 'shop_id' => $shop_id, 'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=> date('Y-m-d H:i:s'));
        }
        // dd($d);
        IssuedCoupons::insert($d);
        $shop = Shop::find($shopId);
        $shop->available_coupons = $shop->available_coupons - $numberOfCoupns;
        $shop->save();
        return true;
    }

}


