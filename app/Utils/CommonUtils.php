<?php
namespace App\Utils;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CommonUtils {
    public static function getCurrentUser(): User {
        return Auth::user();
    }

    public static function isAdmin(): bool {
        /** @var \App\User|null $user */
        $user = Auth::user();
        return $user->hasRole('admin');
    }

    public static function isShop(): bool {
        /** @var \App\User|null $user */
        $user = Auth::user();
        return $user->hasRole('shop');
    }

    public static function getShop() {
        return Shop::with('user')->where('user_id',Auth::user()->id )->first();
    }

    public static function getSettings($key) {
        return config('constants.settings')[$key];
    }

    public static function generateOtp($n = 4) {
        $generator = "1357902468";
        $result = "";
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
        return $result;
    }

}


