<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IssuedCoupons extends Model
{
    use HasFactory;
    protected $guarded = [];
}
