<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssuedCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_coupons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('coupon_issue_id');
            $table->unsignedBigInteger('coupon_id');
            $table->timestamps();

            $table->foreign('coupon_issue_id')
                ->references('id')
                ->on('coupon_issues');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_coupons');
    }
}
