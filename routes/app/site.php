<?php

use App\Http\Livewire\Site\Home;
use App\Http\Livewire\Auth\ShopLogin;
use App\Http\Livewire\Site\Login;
use App\Http\Livewire\User\HomeLogin;
use App\Http\Livewire\User\Products;
use App\Http\Livewire\User\Coupens;
use App\Http\Livewire\User\Points;
use App\Http\Livewire\User\AddToCart;
use App\Http\Livewire\Auth\Login  as AdminLogin;
//use App\Http\Livewire\User\Products as ListProduct;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\User\ProductList;
use App\Http\Livewire\User\ViewCart;
use App\Http\Livewire\User\SubmitCart;
use App\Http\Livewire\User\RemoveFromCart;


Route::get('/login', Login::class)->name('login');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['middleware' => 'role:user'], function () {
        Route::get('/', Login::class)->name('site.home');
    });
});

 Route::get('/dashboard', HomeLogin::class)->name('site.login');
  Route::get('/shop/login', ShopLogin::class)->name('shop.login');
 Route::get('/admin/login', AdminLogin::class)->name('admin.login');

  // Route::middleware('guest')->group(function () {
  //   Route::get('admin/login', AdminLogin::class)
  //       ->name('admin.login');});
//  Route::middleware('guest')->group(function () {
//      Route::get('admin/login', Login::class)
//         ->name('admin.login');
//     Route::get('shop/login', ShopLogin::class)
//         ->name('shop.login');


//     // Route::get('register', Register::class)
//     //     ->name('shop.register');
// });
 Route::get('/dashboard/products', Products::class)->name('site.products');
 Route::get('/dashboard/coupens', Coupens::class)->name('site.coupens');
 Route::get('/dashboard/points', Points::class)->name('site.points');
 Route::get('/dashboard/product/listproduct/{id}', Productlist::class)->name('customer.product.list');
 Route::get('/dashboard/product/addtocart/{id}', AddToCart::class)->name('customer.product.addtocart');
 Route::get('/dashboard/product/removefromcart/{id}', RemoveFromCart::class)->name('customer.product.removefromcart');
 Route::get('/dashboard/product/viewcart', ViewCart::class)->name('customer.product.viewcart');
 Route::get('/dashboard/product/submitcart', SubmitCart::class)->name('customer.product.submitcart');
