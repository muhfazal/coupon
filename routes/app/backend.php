<?php

use App\Http\Livewire\CouponIssues\Issue;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\User\ListUser;
use App\Http\Livewire\CouponRequest\Create as RequestCoupon;
use App\Http\Livewire\CouponRequest\Index as RequestCouponIndex;
use App\Http\Livewire\Shop\Create as ShopCreate;
use App\Http\Livewire\Shop\Edit as ShopEdit;
use App\Http\Livewire\Shop\Index as ShopIndex;
use App\Http\Livewire\Product\Create as ProductCreate;
use App\Http\Livewire\Product\Edit as ProductEdit;
use App\Http\Livewire\Product\Index as ProductIndex;
use App\Http\Livewire\Drawcoupons\Drawcoupon as Draw;
use App\Http\Livewire\Drawcoupons\Viewcoupon as View;
use App\Http\Livewire\Drawcoupons\ViewPurchase as Viewpurchase;
use App\Http\Livewire\User\UserDetails;
use App\Models\Shop;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\CouponIssues\UserController;


Route::group(['middleware' => ['auth','get.menu','role:admin|shop'], 'prefix'=>'backend'], function () {
    // livewire routes for backend

    Route::get('/', Dashboard::class)->name('backend.home');
    Route::get('/requests', RequestCouponIndex::class)->name('backend.requests');
    Route::get('/coupons', Issue::class)->name('backend.coupon.issue');


    // routes for admin only
    Route::group(['namepspace' => '','middleware'=>['role:admin']], function () {
        Route::get('shops', ShopIndex::class)->name('backend.shops');
        Route::get('shops/create', ShopCreate::class)->name('backend.shops.create');
        Route::get('shops/edit/{id}', ShopEdit::class)->name('backend.shops.edit');
        Route::get('users', ListUser::class)->name('backend.users');
         Route::get('users/userdetails/{id}', UserDetails::class)->name('backend.users.userdetails');
        Route::get('products', ProductIndex::class)->name('backend.products');
        Route::get('products/create', ProductCreate::class)->name('backend.products.create');
        Route::get('products/edit/{id}', ProductEdit::class)->name('backend.products.edit');
       Route::get('drawcoupon', Draw::class)->name('backend.drawcoupon');
       Route::get('viewcoupons', View::class)->name('backend.viewcoupons');
       Route::get('viewpurchase', Viewpurchase::class)->name('backend.viewpurchase');
      

    });

    // livewire routes for shop only
    Route::group(['middleware' => ['role:shop'], 'namespace'=> ''], function () {
         Route::post('post-data', UserController::class)->name('carehomePostData');

        Route::get('/requests/create', RequestCoupon::class)->name('backend.requests.create');
    });

    //non livewire routes
    Route::group(['middleware' => ['role:admin|shop'], 'namespace'=> 'App\Http\Controllers'], function () {
        // routes for both shop and admin

        Route::get('/colors', function () {     return view('backend.colors'); });
        Route::resource('notes', 'NotesController');

// remove after development
        Route::prefix('icon')->group(function () {  // word: "icons" - not working as part of adress
            Route::get('/coreui-icons', function(){         return view('dashboard.icons.coreui-icons'); });
        });
    });
});




// delete after development
Route::group(['middleware' => [], 'prefix'=>'core'], function () {
    Route::get('/', function () { return view('dashboard.homepage'); });
    Route::group(['middleware' => []], function () {
        Route::get('/colors', function () {     return view('dashboard.colors'); });
        Route::get('/typography', function () { return view('dashboard.typography'); });
        Route::get('/charts', function () {     return view('dashboard.charts'); });
        Route::get('/widgets', function () {    return view('dashboard.widgets'); });
        Route::get('/404', function () {        return view('dashboard.404'); });
        Route::get('/500', function () {        return view('dashboard.500'); });
        Route::prefix('base')->group(function () {
            Route::get('/breadcrumb', function(){   return view('dashboard.base.breadcrumb'); });
            Route::get('/cards', function(){        return view('dashboard.base.cards'); });
            Route::get('/carousel', function(){     return view('dashboard.base.carousel'); });
            Route::get('/collapse', function(){     return view('dashboard.base.collapse'); });


            Route::get('/forms', function(){        return view('dashboard.base.forms'); });
            Route::get('/jumbotron', function(){    return view('dashboard.base.jumbotron'); });
            Route::get('/list-group', function(){   return view('dashboard.base.list-group'); });
            Route::get('/navs', function(){         return view('dashboard.base.navs'); });


            Route::get('/pagination', function(){   return view('dashboard.base.pagination'); });
            Route::get('/popovers', function(){     return view('dashboard.base.popovers'); });
            Route::get('/progress', function(){     return view('dashboard.base.progress'); });
            Route::get('/scrollspy', function(){    return view('dashboard.base.scrollspy'); });


            Route::get('/switches', function(){     return view('dashboard.base.switches'); });
            Route::get('/tables', function () {     return view('dashboard.base.tables'); });
            Route::get('/tabs', function () {       return view('dashboard.base.tabs'); });
            Route::get('/tooltips', function () {   return view('dashboard.base.tooltips'); });
        });
        Route::prefix('buttons')->group(function () {
            Route::get('/buttons', function(){          return view('dashboard.buttons.buttons'); });
            Route::get('/button-group', function(){     return view('dashboard.buttons.button-group'); });
            Route::get('/dropdowns', function(){        return view('dashboard.buttons.dropdowns'); });
            Route::get('/brand-buttons', function(){    return view('dashboard.buttons.brand-buttons'); });
        });
        Route::prefix('icon')->group(function () {  // word: "icons" - not working as part of adress
            Route::get('/coreui-icons', function(){         return view('dashboard.icons.coreui-icons'); });
            Route::get('/flags', function(){                return view('dashboard.icons.flags'); });
            Route::get('/brands', function(){               return view('dashboard.icons.brands'); });
        });
        Route::prefix('notifications')->group(function () {
            Route::get('/alerts', function(){   return view('dashboard.notifications.alerts'); });
            Route::get('/badge', function(){    return view('dashboard.notifications.badge'); });
            Route::get('/modals', function(){   return view('dashboard.notifications.modals'); });
        });
        Route::resource('notes', 'NotesController');
    });
});
