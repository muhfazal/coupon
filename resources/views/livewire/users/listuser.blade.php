<div class="container-fluid" x-data="{toDelete: '', deleteId: ''}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="flex flex-row justify-between">
                            <div class="flex-1">
                                <h3>Users</h3>
                            </div>
                            <div class="mr-4">
                               
                            </div>


                            <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton"
                                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="">Create new shop</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                   
                      
                      <!-- design -->

                      <table class="table mb-0 table-responsive-sm table-hover table-outline">
                        <thead class="thead-light">
                          <tr>
                            <th>
                              <svg class="c-icon">
                                <use xlink:href="{{ url('/assets/icons/coreui/free-symbol-defs.svg#cui-people')}}"></use>
                              </svg>
                            </th>
                            <th>User</th>
                            <th class="text-center">Phone</th>
                          
                          </tr>
                        </thead>
                        <tbody>

                        <!-- loopstart -->
                        @if(count(array($lists)))
                         @foreach($lists as $list)

                         <tr>
                            <td class="text-center">
                              <div class="c-avatar"><img class="c-avatar-img" src="{{ url('/assets/img/avatars/1.jpg') }}" alt="user@email.com"><span class="c-avatar-status bg-success"></span></div>
                            </td>
                            <td>
                              <div><a href="{{URL::to('backend/users/userdetails',$list->id)}}"> {{$list->name}}</a></div>
                              
                            </td>
                            <td class="text-center"><i class="flag-icon flag-icon-us c-icon-xl" id="us" title="us"></i>{{$list->phone}}</td>
                            <td>
                              
                          </tr>
                          @endforeach
                          <tr >
                           <td colspan="3" >{{$lists->links()}}</td></tr>
                          @endif

                        <!-- loopend -->
                        </tbody>
                      </table>

                    <!-- design -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Delete - <span x-text="toDelete" /></h4>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete <span x-text="toDelete" /></p>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
              <button class="btn btn-danger" data-dismiss="modal" x-on:click="$wire.delete(deleteId)" type="button">Yes, I'm pretty sure</button>
            </div>
          </div>
          <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
      </div>

</div>
