  
<div class="container-fluid" x-data="{toDelete: '', deleteId: ''}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="flex flex-row justify-between">
                            <div class="flex-1">
                                <h3> Active coupons : {{$couponcount}}</h3>
                            </div>
                            <div class="mr-4">
                               
                            </div>

                          
                            <!-- <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton"
                                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="">Create new shop</a>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <!-- <div class="card-body"> -->
                    <!--     <table class="table table-responsive-sm">
                            <thead>
                                <tr>
                                    <th> Active coupons</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                              @foreach($shop as $shops)
                              <b>{{$shops->name}}</b>
                               @foreach($coupon as $key=>$coupons)
                               <div class="row">
                             @if($coupons->shop_id == $shops->id)
  
                                {{$coupons->code}}    
                                            
                                @endif
                                </div>
                                @endforeach
                                @endforeach        
                                  </td>
                                  </tr>

                               
                            </tbody>
   
                       </table>
 -->
<!-- design test -->

 @foreach($shop as $shops)
<div class="card">
<div class="card-header"><strong>{{$shops->name}}</strong></div>
<div class="card-body">
<div class="row align-items-center">
<!-- <div class="col-12 mb-3 mb-xl-0">Active coupons</div> -->
@foreach($coupon as $key=>$coupons)
 @if($coupons->shop_id == $shops->id)
<div class="col-6 col-sm-4 col-md-2 mb-3 ">
<button class="btn btn-block btn-success" type="button">{{$coupons->code}} </button>
</div>
 @endif
 @endforeach
    
</div>
</div>
</div>

@endforeach


 <!-- exp cpn -->

<div class="card">
                    <div class="card-header">
                        <div class="flex flex-row justify-between">
                            <div class="flex-1">
                                <h3> Expired coupons : {{$expcouponcount}}</h3>
                            </div>
                            <div class="mr-4">
                               
                            </div>

                          
                           
                        </div>
                    </div>
 @foreach($expshop as $expshops)
<div class="card">
<div class="card-header"><strong>{{$expshops->name}}</strong></div>
<div class="card-body">
<div class="row align-items-center">
<!-- <div class="col-12  mb-3 mb-xl-0">Expired coupons</div> -->
@foreach($expcoupon as $key=>$expcoupons)
 @if($expcoupons->shop_id == $expshops->id)
<div class="col-6 col-sm-4 col-md-2  mb-3 ">
<button class="btn btn-block btn-danger" type="button">{{$expcoupons->code}} </button>
</div>
 @endif
 @endforeach
    
</div>
</div>
</div>

@endforeach





     <!-- expcpn -->






                       <!-- <div class="card-body">
                        <table class="table table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Expired coupons</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                              @foreach($expshop as $expshops)
                            <b>  {{$expshops->name}}</b>
                               @foreach($expcoupon as $key=>$expcoupons)
                               <div class="row">
                             @if($expcoupons->shop_id == $expshops->id)
  
                                {{$expcoupons->code}}    
                                            
                                @endif
                                </div>
                                @endforeach
                                @endforeach        
                                  </td>
                                  </tr>

                               
                            </tbody>
                        </table>
 -->
                        <!-- testdesign -->
                    <!--  <div class="card">
 <div class="card-header"><strong>Shop name</strong></div>
<div class="card-body">
<div class="row align-items-center">
<div class="col-12 col-xl mb-3 mb-xl-0">Normal</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-primary" type="button">Primary</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-secondary" type="button">Secondary</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-success" type="button">Success</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-warning" type="button">Warning</button>
</div> 
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-danger" type="button">Danger</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-info" type="button">Info</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-light" type="button">Light</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-dark" type="button">Dark</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-link" type="button">Link</button>
</div>
</div>
<div class="row align-items-center mt-3">
<div class="col-12 col-xl mb-3 mb-xl-0">Active State</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-primary active" type="button" aria-pressed="true">Primary</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-secondary active" type="button" aria-pressed="true">Secondary</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-success active" type="button" aria-pressed="true">Success</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-warning active" type="button" aria-pressed="true">Warning</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-danger active" type="button" aria-pressed="true">Danger</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-info active" type="button" aria-pressed="true">Info</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-light active" type="button" aria-pressed="true">Light</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-dark active" type="button" aria-pressed="true">Dark</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-link active" type="button" aria-pressed="true">Link</button>
</div>
</div>
<div class="row align-items-center mt-3">
<div class="col-12 col-xl mb-3 mb-xl-0">Disabled</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-primary" type="button" disabled="">Primary</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-secondary" type="button" disabled="">Secondary</button>
</div>
 <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-success" type="button" disabled="">Success</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-warning" type="button" disabled="">Warning</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-danger" type="button" disabled="">Danger</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-info" type="button" disabled="">Info</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-light" type="button" disabled="">Light</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-dark" type="button" disabled="">Dark</button>
</div>
<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
<button class="btn btn-block btn-link" type="button" disabled="">Link</button>
</div>
</div>
</div>
</div>     -->
       <!-- design test -->
                    </div>

                </div>
            