
<div class="container-fluid" x-data="{couponSet: @entangle('couponSet')}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h3>Create New Request</h3></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="POST">
                            <div>
                                <div class="form-group row">
                                    <label class="col-form-label" for="shopName">Number of sets</label>
                                    <div class="controls col-lg-6">
                                        <input class="form-control @error('numberOfSet') is-invalid @enderror" wire:model="numberOfSet" type="text" placeholder="Number of sets">
                                        @error('numberOfSet') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                    <!-- <div class="col-lg-2">
                                        <span class="row"> * <span class="text-info">{{$couponSet}}</span>
                                    </div> -->
                                </div>
                            </div>
                            <div class="mt-5 mb-5">
                                <p> Request <span class="text-info">{{(int)$numberOfSet}}</span> copuns<p>
                            </div>





                            <div class="form-actions">
                                <button class="btn btn-primary" wire:click="submit" type="button">Submit</button>
                                <button class="btn btn-secondary" type="button">Cancel</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
</div>
