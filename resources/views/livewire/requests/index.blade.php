<div class="container-fluid" x-data="{toDelete: '', deleteId: ''}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="flex flex-row justify-between">
                            <div class="flex-1">
                                <h3>{{ $header }}</h3>
                            </div>
                            <div class="mr-4">
                                {{ $requests->links() }}
                            </div>


                            <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton"
                                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('backend.requests.create')}}">Create new request</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>No of sets</th>
                                    <th>status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($requests as $request)
                                    <tr>
                                        <td>{{ $request->no_of_sets }}</td>
                                        <td>{{ $request->status }}</td>
                                        <td>@role('shop')
                                            @if($request->status === "PENDING")
                                            <button class="btn btn-danger"
                                            @click="deleteId='{{$request->id}}'"
                                            type="button" data-toggle="modal" data-target="#dangerModal"
                                            >
                                            <i class="c-icon cil-trash"></i>
                                            </button>
                                            @endif
                                            @endrole
                                            @role('admin')
                                            @if($request->status === "PENDING")
                                            <button class="btn btn-success"
                                            @click="acceptId='{{$request->id}}'"
                                            type="button" wire:click="acceptRequest({{$request->id}})"
                                            >
                                            <i class="c-icon cil-check-alt"></i>
                                            </button>
                                            @endif
                                            @endrole
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $requests->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Delete</h4>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this</p>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
              <button class="btn btn-danger" data-dismiss="modal" x-on:click="$wire.delete(deleteId)" type="button">Yes, I'm pretty sure</button>
            </div>
          </div>
          <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
      </div>

</div>
