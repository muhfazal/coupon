<div class="container-fluid" x-data="{toDelete: '', deleteId: ''}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="flex flex-row justify-between">
                            <div class="flex-1">
                                <h3></h3>
                            </div>
                            <div class="mr-4">
                               
                            </div>


                            <!-- <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton"
                                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="">Create new shop</a>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                      <form method="POST">
                    <div class="card-body">
                        <table class="table table-responsive-sm">
                            <thead>
                                <tr>
                                
                                  <th>Date</th>
                                  <th>Coupon</th>
                                  <th>Winner phone</th>
                              </tr>
                            </thead>
                            <tbody>
                             
                                    <tr>
                                        <td>
                                     <input type="date" max="<?php echo date('Y-m-d'); ?>" wire:model="coupon_date">


                                       <button type="button" wire:click="drawcoupon">Click here</button> </td>
                                        @if($coupon)
                                        <td>
                                           {{$coupon->code}}
                                        </td>
                                        <td>
                                            {{$coupon->phone}}
                                        </td>
                                        @endif
                                    </tr>
                              
                            </tbody>
                        </table>

                        
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
<!--     <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Delete - <span x-text="toDelete" /></h4>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete <span x-text="toDelete" /></p>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
              <button class="btn btn-danger" data-dismiss="modal" x-on:click="$wire.delete(deleteId)" type="button">Yes, I'm pretty sure</button>
            </div>
          </div>
          <!-- /.modal-content-->
        <<!-- /div> -->
        <!-- /.modal-dialog-->
      <<!-- /div> --> -->

</div>
