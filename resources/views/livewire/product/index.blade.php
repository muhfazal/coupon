<div class="container-fluid" x-data="{toDelete: '', deleteId: ''}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="flex flex-row justify-between">
                            <div class="flex-1">
                                <h3>{{ $header }}</h3>
                            </div>
                            <div class="mr-4">
                                {{ $products->links() }}
                            </div>


                            <div class="btn-group">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton"
                                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('backend.products.create')}}">Add new product</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Images</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->description }}</td>
                                        <td>
                                            @foreach($product->images as $image)
                                                <img src="{{Storage::disk('s3')->url($image->image)}}" width="100" height="100" />
                                            @endforeach
                                        </td>
                                        <td>
                                            <button class="btn btn-danger"
                                            @click="toDelete = '{{$product->name}}'; deleteId='{{$product->id}}'"
                                            type="button" data-toggle="modal" data-target="#dangerModal"
                                            >
                                            <i class="c-icon cil-trash"></i>
                                            </button>
                                            <button class="btn btn-info"
                                            wire:click="redirectToEdit({{$product->id}})"
                                            type="button"
                                            >
                                            <i class="c-icon cil-pencil"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Delete - <span x-text="toDelete" /></h4>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete <span x-text="toDelete" /></p>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
              <button class="btn btn-danger" data-dismiss="modal" x-on:click="$wire.delete(deleteId)" type="button">Yes, I'm pretty sure</button>
            </div>
          </div>
          <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
      </div>

</div>
