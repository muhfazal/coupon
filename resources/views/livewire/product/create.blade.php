
<div class="container-fluid">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h3>Add New Product</h3></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="POST">
                            <div class="form-group">
                                <label class="col-form-label" for="productName">Product name</label>
                                <div class="controls">
                                    <input class="form-control @error('productName') is-invalid @enderror" wire:model="productName" type="text" placeholder="Product name">
                                    @error('productName') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="description">Description</label>
                                <div class="controls">
                                    <textarea class="form-control @error('description') is-invalid @enderror" wire:model="description"  rows="4" placeholder="Description"></textarea>
                                    @error('description') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="pointsNeeded">Points needed</label>
                                <div class="controls">
                                    <input class="form-control @error('pointsNeeded') is-invalid @enderror" wire:model="pointsNeeded" type="text" placeholder="Points needed">
                                    @error('pointsNeeded') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-form-label" for="images">Images</label>
                                <div class="controls">
                                    <input id="images" type="file" name="images[]" multiple="" wire:model="images">
                                    @error('images.*') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-form-label" for="isEnabled">Is Enabled?</label>
                                <div class="controls">
                                    <label class="c-switch c-switch-label c-switch-success">
                                        <input class="c-switch-input" type="checkbox" wire:model="isEnabled"><span class="c-switch-slider" data-checked="Yes" data-unchecked="No"></span>
                                      </label>
                                    @error('isEnabled') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>



                            <div class="form-actions">
                                <button class="btn btn-primary" wire:click="submit" type="button">Save changes</button>
                                <button class="btn btn-secondary" type="button">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
</div>
