
<div class="container-fluid">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h3>Create New Shop</h3></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="POST">
                            <div class="form-group">
                                <label class="col-form-label" for="shopName">Shop name</label>
                                <div class="controls">
                                    <input class="form-control @error('shopName') is-invalid @enderror" wire:model="shopName" type="text" placeholder="Shop name">
                                    @error('shopName') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="address">Address</label>
                                <div class="controls">
                                    <textarea class="form-control @error('address') is-invalid @enderror" wire:model="address"  rows="4" placeholder="Address"></textarea>
                                    @error('address') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="email">Email</label>
                                <div class="controls">
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="phone">Phone</label>
                                <div class="controls">
                                    <input class="form-control @error('phone') is-invalid @enderror" wire:model="phone"  type="text" placeholder="Enter phone number">
                                    @error('phone') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="password">Password</label>
                                <div class="controls">
                                    <input class="form-control @error('password') is-invalid @enderror" wire:model="password" type="password" placeholder="Enter password">
                                    @error('password') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="re-password">Re-enter password</label>
                                <div class="controls">
                                    <input class="form-control" wire:model="password_confirmation" type="password" placeholder="Re-enter password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="re-password">Number of coupons</label>
                                <div class="controls">
                                    <input class="form-control" wire:model="coupons" type="text" placeholder="Number of Coupons">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-primary" wire:click="submit" type="button">Save changes</button>
                                <button class="btn btn-secondary" type="button">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
</div>
