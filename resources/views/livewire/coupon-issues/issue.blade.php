
<div class="container-fluid" x-data="{couponSet: @entangle('couponSet')}">
    @include('livewire.includes.messages')
    <div class="fade-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="flex justify-between card-header">
                        <h3>Issue Coupons</h3>
                        <p class="pull-right">Available Coupons <span class="text-info"><strong>{{$availableCoupons}}</strong></span></p>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" method="POST">
                            <div>
                                <div class="form-group">
                                    <label class="col-form-label" for="shopName">Phone</label>
                                    <div class="controls">
                                        <input class="form-control @error('phone') is-invalid @enderror" wire:model="phone" wire:keyup="username"    type="text" placeholder="Phone Number" id="ph_change" >
                                        @error('phone') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-form-label" for="userName">Name</label>
                                    <div class="controls">
                                        <input class="form-control" wire:model="userName" type="text" placeholder="Name">
                                       
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-form-label" for="shopName">Number of coupons</label>
                                    <div class="controls">
                                        <input class="form-control @error('numberOfCoupons') is-invalid @enderror" wire:model="numberOfCoupons" type="text" placeholder="Number of coupons">
                                        @error('numberOfCoupons') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions">

                                <input type="text" name="id" >
                            <input type="hidden"  name="type" value="check_phone_no">
                                <button class="btn btn-primary" wire:click="issue" type="button">Submit</button>
                                <button class="btn btn-secondary" type="button">Cancel</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
   
 // function myFunction() {
 //    var phone=document.getElementById('phone').value;
    
  
 // }
 // $('#ph_change').keyup(function(){
 //   var ph=$('#ph_change').val();
 //   // alert(ph);

 //   $.ajax({
 //            url: '{{ URL::route("carehomePostData") }}',
 //            type: 'post',
 //            data: {_token:'{{ csrf_token() }}',type:'check_phone_no',phone_no:ph},

 //            success: function(data){
 //                if(data.statusCode == 6000){
 //                    alert('success');
 //                }else{
 //                    alert('failed');
 //                }
 //            }
 //    });
 // });




</script>