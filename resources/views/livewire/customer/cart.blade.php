
<div class="container" >

  <div class="section" >
    <div class="row " >
      <div class="col s12 pad-0">
      <br><br>

        <h5 class="bot-20 sec-tit  ">C<span>art Items</span>
          <!-- <a href="{{URL::to('dashboard/product/viewcart')}}" > <span style="float:right;">   Cart({{$cart}})</span> </a> -->
           <button id="btnpurchase" class="addtocart btn-small" style="float:right;">Purchase</button>
        </h5>
      <div class="row ui-mediabox  prods prods-boxed" id="bg-row">
        @if(count($cart))
			@foreach($cart as $product)
			  <div class="col s6">    
			        <div class="prod-img-wrap">
			            <a class="img-wrap" href="/alix/assets/images/product-1.jpg" data-fancybox="images" data-caption="Women Shoes">
			              <img class="z-depth-1" style="width: 100%;"  src="{{ asset('/alix/assets/images/events-app.jpg')}}">
			            </a>      
			  </div>
			  <div class="prod-info  boxed z-depth-1">
			   <a href="{{URL::to('dashboard/product/listproduct',$product->id)}}" > <h5 class="title truncate">{{$product->name}} </h5> </a> 
			    <span class="small date">Addy</span>
			    <div class="spacer-line"></div>
			      <h5 class="bot-0 price">{{$product->points_needed}}</h5>
			      <div class="spacer-line"></div>
			      <div class='prod-options'>
			        <div class='size'>
			           <span>6</span><span>7</span><span></span><span>9</span>
			        </div>
			        <div class='color'>
			         <span style='background-color:#f06292;'>&nbsp;</span>
			         <span style='background-color:#4db6ac;'>&nbsp;</span>
			         <span style='background-color:#aed581;'>&nbsp;</span>
			        </div>
			      </div>
			      <div class="spacer-line"></div>
			        <a href="{{URL::to('dashboard/product/removefromcart',$product->product_id)}}" > 
			         <span class="addtocart btn-small" >Remove from cart</span>
			        </a>
			        <span class="addtowishlist btn-small">Wishlist</span>   
			      <div class="spacer-line"></div>
			     </div>
			  </div>
			@endforeach
			 <div class="col-12 text-center">

		        <!-- <a href="{{URL::to('/dashboard/product/submitcart')}}" style="display: none;"><span class="addtocart btn-small" >Submit Cart</span></a> -->
		     </div>
		@else
		     <div class="col-12 text-center">
		        <span class="addtocart btn-small" >Cart is empty</span>
		     </div>		 
		@endif
		    
		<br><br>

      </div>

   </div>

  </div>

 </div>
<!-- ************************* address modal ******************************** -->
<div class="container row">
 <div class="col-12 text-center">
	 <div class="address_modal" id="addressmodal">
   <!--   <input type="text" wire:model="username" placeholder="Enter name"><br>
     <input type="text" wire:model="first_address" placeholder="First line of address"><br>

     <input type="text" wire:model="last_address" placeholder="Second line of address"></br>
     <input type="text" wire:model="postcode" placeholder="Enter pin"><br>
     <input type="text" wire:model="landmark" placeholder="Enter landmark (optional)"> -->
     <input type="text" class="addresstextarea"  wire:model.debounce.30000ms="username" placeholder="Enter name"  id="username">
       <input type="text" class="addresstextarea"  wire:model.debounce.30000ms="firstaddress" placeholder="Enter first line of addesss"  id="firstaddress">
        <input type="text" class="addresstextarea"  wire:model.debounce.30000ms="lastaddress" placeholder="Enter second line of addesss"  id="secondaddress">
         <input type="text" class="addresstextarea"  wire:model.debounce.30000ms="postcode" placeholder="Enter postcode"  id="postcode">
          <input type="text" class="addresstextarea"  wire:model.debounce.30000ms="landmark" placeholder="Enter landmark (optional)"  id="lamdmarkname">

		 <!-- <textarea class="addresstextarea" type="text" wire:model.debounce.30000ms="address" placeholder="Enter address and continue" rows="4" cols="50" id="textareaaddress"></textarea> -->
		 <p class="text-red" id="error_msg">Enter a valid address</p>
		 <button class="addtocart btn-small addbtn" type="button" id="btnsubmit">Submit</button>
		 <button class="addtocart btn-small addbtn" type="button" wire:click="addaddress" style="display:none;" id="cartsubmit">Submit</button>
		</div>
 </div>
</div>
<!-- ************************* address modal ******************************** -->
</div> 

<!-- 
@livewireScripts -->
<script
  src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
  integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
  crossorigin="anonymous">
  	
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	$( document ).ready(function() {
		if('{{$msg}}'){
          swal('{{$msg}}');	
		}
		$('#btnpurchase').click(function(){
          $('#addressmodal').css('display','block');
          $('#bg-row').hide();
		});
		// $('#textareaaddress').keyup(function(){
		// 	$('#error_msg').hide();
		// });
		$('#btnsubmit').click(function(){
			$( "#cartsubmit" ).trigger( "click" );
		 // var address=$('#textareaaddress').val();
		 // var adddres2=address.replace(/\s+/g, '');
		 // if(adddres2.length<10){
		 // 	$('#error_msg').show();
		 // }
		 // else{
		 // 	$( "#cartsubmit" ).trigger( "click" );
		 // }
	    });
    });
</script>