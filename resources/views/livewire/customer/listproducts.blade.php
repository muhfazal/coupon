<nav class="fix_topscroll logo_on_fixed  topbar navigation" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="index.html" class=" brand-logo " >Alix</a>    <!-- <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul> -->

    <!-- <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Navbar Link</a></li>
      </ul> -->
    <a href="#" data-target="" class="waves-effect waves-circle navicon back-button htmlmode show-on-large "><i
        class="mdi mdi-arrow-left" data-page=""></i></a>

    <a href="#" data-target="slide-nav" class="waves-effect waves-circle navicon sidenav-trigger show-on-large"><i class="mdi mdi-menu"></i></a>



    <a href="#" data-target="slide-settings" class="waves-effect waves-circle navicon right sidenav-trigger show-on-large pulse"><i
        class="mdi mdi-settings-outline"></i></a>

    <a href="#" data-target="" class="waves-effect waves-circle navicon right nav-site-mode show-on-large"><i
        class="mdi mdi-invert-colors mdi-transition1"></i></a>
    <!-- <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a> -->
  </div>
</nav>



<div class="container">
    <div class="section">
      <h5 class="pagetitle">Product View</h5>
          </div>
  </div>
  




  
  <div class="container">
    <div class="section">

     






<div
  class="row ui-mediabox  prods prods-boxed    ">

  
  <div class="col s12">
        <div class="prod-img-wrap">

            <a class="img-wrap" href="alix/assets/images/product-1.jpg" data-fancybox="images"
        data-caption="Women Shoes">
        <img class="z-depth-1" style="width: 100%;" src="/alix/assets/images/product-1.jpg">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="ui-app-products-view.html" >      <h5 class="title truncate">{{$product->name}}      </h5>
      </a>      <span class="small date">Addy</span>
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">{{$product->points_needed}}</h5>
                  <div class="spacer-line"></div>

      <div class='prod-options'><div class='size'><span>6</span><span>7</span><span>8</span><span>9</span></div><div class='color'><span style='background-color:#f06292;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#aed581;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>

      
        <span class="addtocart btn-small">Add to cart</span>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      
        <p class="bot-0 text">{{$product->description}}
        </p>

        <div class="spacer"></div>

      

          </div>
  </div>
  


</div>

    </div>
  </div>




<footer class="page-footer center social-colored ">
  <div class="container footer-content">
    <div class="row">
      <div class="">
        <h5 class="logo">ALIX</h5>
                <p class="text">Alix has tons of features and functionalities making it an ultimate PWA solution.</p>
              </div>
      <div class="link-wrap">

                <ul class="social-wrap">
          <li class="social">
            <a class="" href="#!"><i class="mdi mdi-facebook"></i></a>
            <a class="" href="#!"><i class="mdi mdi-twitter"></i></a>
            <a class="" href="#!"><i class="mdi mdi-dribbble"></i></a>
            <a class="" href="#!"><i class="mdi mdi-google-plus"></i></a>
            <a class="" href="#!"><i class="mdi mdi-linkedin"></i></a>

          </li>
        </ul>
      </div>
    </div>
  </div>

    <div class="footer-copyright">
    <div class="container">
      &copy; Copyright <a class="" href="https://themeforest.net/user/themepassion/portfolio">Themepassion</a>. All rights reserved.
    </div>
  </div>
  </footer>



<div class="backtotop">
  <a class="btn-floating btn primary-bg">
    <i class="mdi mdi-chevron-up"></i>
  </a>
</div>





<div class="footer-menu circular">
  <ul>
      <li>
      <a href="ui-apps.html" >      <i class="mdi mdi-open-in-app"></i>
      <span>Apps</span>
      </a>    </li>
        <li>
      <a href="sub-pages.html" >      <i class="mdi mdi-palette-swatch"></i>
      <span>Pages</span>
      </a>    </li>
        <li>
      <a href="sub-home.html" >      <i class="mdi mdi-home-outline"></i>
      <span>Home</span>
      </a>    </li>
        <li>
      <a href="sub-components.html" >      <i class="mdi mdi-flask-outline"></i>
      <span>Components</span>
      </a>    </li>
        <li>
      <a href="sub-gallery.html" >      <i class="mdi mdi-view-dashboard"></i>
      <span>Gallery</span>
      </a>    </li>
      
  </ul>
</div>








<!-- PWA Service Worker Code -->

<script type="text/javascript">
// This is the "Offline copy of pages" service worker

// Add this below content to your HTML page, or add the js file to your page at the very top to register service worker

// Check compatibility for the browser we're running this in
if ("serviceWorker" in navigator) {
  if (navigator.serviceWorker.controller) {
    console.log("[PWA Builder] active service worker found, no need to register");
  } else {
    // Register the service worker
    navigator.serviceWorker
      .register("pwabuilder-sw.js", {
        scope: "./"
      })
      .then(function(reg) {
        console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
      });
  }
}
</script>
<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

<!-- CORE JS FRAMEWORK - START -->
<script src="modules/jquery/jquery-2.2.4.min.js"></script>
<script src="modules/materialize/materialize.js"></script>
<script src="modules/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="assets/js/variables.js"></script>
<!-- CORE JS FRAMEWORK - END -->


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<script src="modules/fancybox/jquery.fancybox.min.js" type="text/javascript"></script>
<script type="text/javaScript">
  $("[data-fancybox=images]").fancybox({
  buttons : [ 
    "slideShow",
    "share",
    "zoom",
    "fullScreen",
    "close",
    "thumbs"
  ],
  thumbs : {
    autoStart : false
  }
});
</script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


<!-- CORE TEMPLATE JS - START -->
<script src="modules/app/init.js"></script>
<script src="modules/app/settings.js"></script>

<script src="modules/app/scripts.js"></script>

<!-- END CORE TEMPLATE JS - END -->


<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
  $('.preloader-background').delay(10).fadeOut('slow');
});
</script>
</body>

</html>