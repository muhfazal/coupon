<div class="carousel carousel-fullscreen carousel-slider home_carousel carousel-products-home">

        <div class="carousel-item" href="#carousel-slide-0!">
      <div class="bg" style="background-image: url('assets/images/product-19.jpg')"></div>
      <div class="item-content center-align white-text">
        <div class="spacer-large"></div>
        <h3>Weekend Sale</h3>
        <h5 class="light white-text">Make the best use today</h5>
      </div>
    </div>
        <div class="carousel-item" href="#carousel-slide-1!">
      <div class="bg" style="background-image: url('assets/images/product-20.jpg')"></div>
      <div class="item-content left-align white-text">
        <div class="spacer-large"></div>
        <h3>New Deals and Offers</h3>
        <h5 class="light white-text">Get the best products at discount prices</h5>
      </div>
    </div>
        <div class="carousel-item" href="#carousel-slide-2!">
      <div class="bg" style="background-image: url('assets/images/product-2.jpg')"></div>
      <div class="item-content center-align white-text">
        <div class="spacer-large"></div>
        <h3>Original Branded Products</h3>
        <h5 class="light white-text">Upto 70% off products</h5>
      </div>
    </div>
        <div class="carousel-item" href="#carousel-slide-3!">
      <div class="bg" style="background-image: url('assets/images/product-9.jpg')"></div>
      <div class="item-content right-align white-text">
        <div class="spacer-large"></div>
        <h3>Best Discounts and Offers</h3>
        <h5 class="light white-text">Make your wishlist today</h5>
      </div>
    </div>
    
  </div>

<div class="container">
    <div class="section">

    <div class="row ">
                <div class="col s12 pad-0">
                  <h5 class="bot-20 sec-tit  ">P<span>remium Products</span>
                   <a href="{{URL::to('dashboard/product/viewcart')}}" > <span style="float:right;">   Cart({{$cart}})</span> </a>
                  </h5>
     <div class="row ui-mediabox  prods prods-boxed">

  @foreach($products as $product)
  <div class="col s6">
   
        <div class="prod-img-wrap">

            <a class="img-wrap" href="/alix/assets/images/product-1.jpg" data-fancybox="images"
        data-caption="Women Shoes">
        <img class="z-depth-1" style="width: 100%;"  src="{{ asset('/alix/assets/images/events-app.jpg')}}">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="{{URL::to('dashboard/product/listproduct',$product->id)}}" >      <h5 class="title truncate">{{$product->name}}    </h5>
      </a>    <!--   <span class="small date">Addy</span> -->
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">Points needed :{{$product->points_needed}}</h5>
@if($product->points_needed > $userpoints->total_points)
<p style="color: red;">You need {{$product->points_needed -  $userpoints->total_points }} more points to buy this product</p>
         <div class="spacer-line"></div>

     <!--  <div class='prod-options'><div class='size'><span>6</span><span>7</span><span>8</span><span>9</span></div><div class='color'><span style='background-color:#f06292;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#aed581;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>
 -->
      
       
        
      
      <div class="spacer-line"></div>
      

          </div>
          
  </div>

@else 



                  <div class="spacer-line"></div>

      <!-- <div class='prod-options'><div class='size'><span>6</span><span>7</span><span>8</span><span>9</span></div><div class='color'><span style='background-color:#f06292;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#aed581;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>
 -->
      
        <a href="{{URL::to('dashboard/product/addtocart',$product->id)}}" disable> <span class="addtocart btn-small" >Add to cart
      </span></a>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      

          </div>
          
  </div>
  @endif
  @endforeach


  {{$products->links()}}
  <!-- <div class="col s6">
        <div class="prod-img-wrap">

            <a class="img-wrap" href="assets/images/product-2.jpg" data-fancybox="images"
        data-caption="Home Decor Balls">
        <img class="z-depth-1" style="width: 100%;" src="assets/images/product-2.jpg">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="ui-app-products-view.html" >      <h5 class="title truncate">Home Decor Balls      </h5>
      </a>      <span class="small date">Linda</span>
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">$39</h5>
                  <div class="spacer-line"></div>

      <div class='prod-options'><div class='options'><span>5 pc.</span><span>7 pc.</span><span>9 pc.</span></div><div class='color'><span style='background-color:#aed581;'>&nbsp;</span><span style='background-color:#ff8a65;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>

      
        <span class="addtocart btn-small">Add to cart</span>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      

          </div>
  </div>
  
  <div class="col s6">
        <div class="prod-img-wrap">

            <a class="img-wrap" href="assets/images/product-3.jpg" data-fancybox="images"
        data-caption="Women Sandals">
        <img class="z-depth-1" style="width: 100%;" src="assets/images/product-3.jpg">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="ui-app-products-view.html" >      <h5 class="title truncate">Women Sandals      </h5>
      </a>      <span class="small date">Addy</span>
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">$35</h5>
                  <div class="spacer-line"></div>

      <div class='prod-options'><div class='size'><span>6</span><span>7</span><span>8</span><span>9</span></div><div class='color'><span style='background-color:#f06292;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#ff8a65;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>

      
        <span class="addtocart btn-small">Add to cart</span>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      

          </div>
  </div>
  
  <div class="col s6">
        <div class="prod-img-wrap">

            <a class="img-wrap" href="assets/images/product-4.jpg" data-fancybox="images"
        data-caption="Modern Man Shoes">
        <img class="z-depth-1" style="width: 100%;" src="assets/images/product-4.jpg">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="ui-app-products-view.html" >      <h5 class="title truncate">Modern Man Shoes      </h5>
      </a>      <span class="small date">Reeboo</span>
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">$53</h5>
                  <div class="spacer-line"></div>

      <div class='prod-options'><div class='size'><span>6</span><span>7</span><span>8</span><span>9</span></div><div class='color'><span style='background-color:#ff8a65;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#aed581;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>

      
        <span class="addtocart btn-small">Add to cart</span>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      

          </div>
  </div>
  
  <div class="col s6">
        <div class="prod-img-wrap">

            <a class="img-wrap" href="assets/images/product-5.jpg" data-fancybox="images"
        data-caption="Wooden Chair">
        <img class="z-depth-1" style="width: 100%;" src="assets/images/product-5.jpg">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="ui-app-products-view.html" >      <h5 class="title truncate">Wooden Chair      </h5>
      </a>      <span class="small date">Pepper</span>
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">$126</h5>
                  <div class="spacer-line"></div>

      <div class='prod-options'><div class='size'><span>1 pc</span><span> 2 pc</span><span> 4 pc</span></div><div class='color'><span style='background-color:#f06292;'>&nbsp;</span><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#ff8a65;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>

      
        <span class="addtocart btn-small">Add to cart</span>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      

          </div>
  </div>
  
  <div class="col s6">
        <div class="prod-img-wrap">

            <a class="img-wrap" href="assets/images/product-6.jpg" data-fancybox="images"
        data-caption="Canvas Shoes">
        <img class="z-depth-1" style="width: 100%;" src="assets/images/product-6.jpg">
      </a>
      
    </div>
        <div class="prod-info  boxed z-depth-1">
      <a href="ui-app-products-view.html" >      <h5 class="title truncate">Canvas Shoes      </h5>
      </a>      <span class="small date">Luna</span>
            <div class="spacer-line"></div>
            <h5 class="bot-0 price">$65</h5>
                  <div class="spacer-line"></div>

      <div class='prod-options'><div class='size'><span>6</span><span>7</span><span>8</span><span>9</span></div><div class='color'><span style='background-color:#4db6ac;'>&nbsp;</span><span style='background-color:#ff8a65;'>&nbsp;</span><span style='background-color:#aed581;'>&nbsp;</span></div></div>      <div class="spacer-line"></div>

      
        <span class="addtocart btn-small">Add to cart</span>

                    <span class="addtowishlist btn-small">Wishlist</span>
        
      
      <div class="spacer-line"></div>
      

          </div>
  </div> -->
  


<!-- </div>

    </div>
    </div> -->
    

       <!--  <div class="row ">
                <div class="col s12 pad-0"><h5 class="bot-20 sec-tit  ">Product categories</h5>    <div class="row settings-row">
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-tablet-cellphone z-depth-1"></i>
        <h6>Technology</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-glasses z-depth-1"></i>
        <h6>Fashion</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-music-box-outline z-depth-1"></i>
        <h6>Entertainment</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-laptop z-depth-1"></i>
        <h6>Electronics</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-cellphone z-depth-1"></i>
        <h6>Mobile</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-account-heart z-depth-1"></i>
        <h6>Beauty</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-sofa z-depth-1"></i>
        <h6>Home Decor</h6>
      </a>
    </div>
  </div>
    <div class="col s6">
    <div class="setting-box z-depth-1 center">
      <a href="ui-app-products-grid2col.html">
        <i class="mdi mdi-dumbbell z-depth-1"></i>
        <h6>Health</h6>
      </a>
    </div>
  </div>
  </div>
    </div>
    </div>
      </div>
</div>

<div class="container">
  <div class="section">


    



<div class="spacer"></div>
<h5 class="center bot-20 sec-tit">Our Customer's Say</h5>


<div class="slider slider8 testi-boxed z-depth-1  white-text primary-bg ">
  <ul class="slides transparent testimonials ">

        <li>
          <p class="center"><i class="mdi mdi-format-quote-open"></i>We are so pleased with the purchase of this product. It has tons of components and features to deal with. You can really create anything you like.<i class="mdi mdi-format-quote-close"></i> </p>
        <div class="center-align">
        <div class="row userinfo">
          <img src="assets/images/user-29.jpg" alt="" class="circle responsive-img">
          <div class="left-align">
            <span class="name"><strong>Mar Leftridge</strong>
              <br><span class='small position'>CEO, Ink Ltd.</span>
            </span>
          </div>
        </div>
      </div>



    </li>
        <li>
          <p class="center"><i class="mdi mdi-format-quote-open"></i>We highly recommend using It for your next project. It is super quality and premium template that you can ask for. Just go for it.<i class="mdi mdi-format-quote-close"></i> </p>
        <div class="center-align">
        <div class="row userinfo">
          <img src="assets/images/user-25.jpg" alt="" class="circle responsive-img">
          <div class="left-align">
            <span class="name"><strong>Glenda Furlonge</strong>
              <br><span class='small position'>Manager, Zed Ind.</span>
            </span>
          </div>
        </div>
      </div>



    </li>
        <li>
          <p class="center"><i class="mdi mdi-format-quote-open"></i>A perfect template to get you going for your next project. A full loaded feature packed template. It is multi purpose and super fast. Thank you for such a wonderful template.<i class="mdi mdi-format-quote-close"></i> </p>
        <div class="center-align">
        <div class="row userinfo">
          <img src="assets/images/user-28.jpg" alt="" class="circle responsive-img">
          <div class="left-align">
            <span class="name"><strong>Gael Burborough</strong>
              <br><span class='small position'>Sr. Designer</span>
            </span>
          </div>
        </div>
      </div>



    </li>
    
  </ul>
</div>


  </div>
</div> -->
</div>
</div>
</div>
</div>
</div>

<div class="spacer"></div>





<footer class="page-footer center social-colored ">
  <div class="container footer-content">
    <div class="row">
      <div class="">
        <h5 class="logo">ALIX</h5>
                <p class="text">Alix has tons of features and functionalities making it an ultimate PWA solution.</p>
              </div>
      <div class="link-wrap">

                <ul class="social-wrap">
          <li class="social">
            <a class="" href="#!"><i class="mdi mdi-facebook"></i></a>
            <a class="" href="#!"><i class="mdi mdi-twitter"></i></a>
            <a class="" href="#!"><i class="mdi mdi-dribbble"></i></a>
            <a class="" href="#!"><i class="mdi mdi-google-plus"></i></a>
            <a class="" href="#!"><i class="mdi mdi-linkedin"></i></a>

          </li>
        </ul>
      </div>
    </div>
  </div>

    <div class="footer-copyright">
    <div class="container">
      &copy; Copyright <a class="" href="https://themeforest.net/user/themepassion/portfolio">Themepassion</a>. All rights reserved.
    </div>
  </div>
  </footer>



<div class="backtotop">
  <a class="btn-floating btn primary-bg">
    <i class="mdi mdi-chevron-up"></i>
  </a>
</div>





<div class="footer-menu circular">
  <ul>
      <li>
      <a href="ui-apps.html" >      <i class="mdi mdi-open-in-app"></i>
      <span>Apps</span>
      </a>    </li>
        <li>
      <a href="sub-pages.html" >      <i class="mdi mdi-palette-swatch"></i>
      <span>Pages</span>
      </a>    </li>
        <li>
      <a href="sub-home.html" >      <i class="mdi mdi-home-outline"></i>
      <span>Home</span>
      </a>    </li>
        <li>
      <a href="sub-components.html" >      <i class="mdi mdi-flask-outline"></i>
      <span>Components</span>
      </a>    </li>
        <li>
      <a href="sub-gallery.html" >      <i class="mdi mdi-view-dashboard"></i>
      <span>Gallery</span>
      </a>    </li>
      
  </ul>
</div>








<!-- PWA Service Worker Code -->

<script type="text/javascript">
// This is the "Offline copy of pages" service worker

// Add this below content to your HTML page, or add the js file to your page at the very top to register service worker

// Check compatibility for the browser we're running this in
if ("serviceWorker" in navigator) {
  if (navigator.serviceWorker.controller) {
    console.log("[PWA Builder] active service worker found, no need to register");
  } else {
    // Register the service worker
    navigator.serviceWorker
      .register("pwabuilder-sw.js", {
        scope: "./"
      })
      .then(function(reg) {
        console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
      });
  }
}
</script>
<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

<!-- CORE JS FRAMEWORK - START -->
<!-- <script src="modules/jquery/jquery-2.2.4.min.js"></script>
<script src="modules/materialize/materialize.js"></script>
<script src="modules/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="assets/js/variables.js"></script> -->
<!-- CORE JS FRAMEWORK - END -->


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<!-- <script src="modules/fancybox/jquery.fancybox.min.js" type="text/javascript"></script> -->
<script src="{{ asset('/alix/modules/fancybox/jquery.fancybox.min.js')}}"> </script>
<script type="text/javaScript">
  $("[data-fancybox=images]").fancybox({
  buttons : [ 
    "slideShow",
    "share",
    "zoom",
    "fullScreen",
    "close",
    "thumbs"
  ],
  thumbs : {
    autoStart : false
  }
});
</script><script type="text/javascript">
  $(document).ready(function(){
      
      $(".carousel-fullscreen.carousel-slider").carousel({
        fullWidth: true,
        indicators: true
      });
      setTimeout(autoplay, 3500);
      function autoplay() {
          $(".carousel").carousel("next");
          setTimeout(autoplay, 3500);
      }
         $(".slider8").slider({
                indicators: false,
                height: 210,
        });

  }); 
    </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


<!-- CORE TEMPLATE JS - START -->
<!-- <script src="modules/app/init.js"></script>
<script src="modules/app/settings.js"></script>
<script src="modules/app/scripts.js"></script> -->

<!-- END CORE TEMPLATE JS - END -->


<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
  $('.preloader-background').delay(10).fadeOut('slow');
});
</script>
</body>

</html>