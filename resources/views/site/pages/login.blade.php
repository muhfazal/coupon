
<div class="container login-area">
    <div class="section">
        <h3 class="bot-20 center white-text">Login</h3>



            <div class="row">
                <div class="input-field col s10 offset-s1">
                    <input id="phone" type="text" class="validate" wire:model="phone">
                    <label for="phone" class="@if($phone) active @endif">Phone</label>
                </div>
                @error('phone')
                <div class="col s10 offset-s1">
                <p class="mb-3 text-sm text-red-600">{{ $message }}</p>
                </div>
                @enderror
            </div>
            @if($isOtpSent)
            <div class="row">
                <div class="input-field col s10 offset-s1">
                    <input id="otp" type="text" class="validate" wire:model="otp">
                    <label for="otp" class="@if($otp) active @endif">OTP</label>
                </div>
                @error('otp')
                <div class="col s10 offset-s1">
                <p class="mb-3 text-sm text-red-600">{{ $message }}</p>
                </div>
                @enderror
            </div>
            @endif



        <div class="row center">
            @if($isOtpSent)
                <a class="waves-effect waves-light btn-large bg-primary" wire:click.prevent="authenticate">Login</a>
            @else
                <button class="waves-effect waves-light btn-large bg-primary" wire:click.prevent="sendOtp">Request OTP</button>
            @endif

            <div class="spacer"></div>
            {{-- <div class="links">
                <a href="ui-pages-forgotpassword.html" class='waves-effect'>Forgot Password</a><span
                    class="sep">|</span><a href="ui-pages-register.html" class='waves-effect'>Register</a> </div> --}}
            <div class="spacer"></div>
            <div class="spacer"></div>
        </div>
    </div>
</div>

