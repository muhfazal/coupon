<div class="ui-profile">
    <div class="primg" style="background-image: url('https://images.unsplash.com/photo-1506617420156-8e4536971650?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1268&q=80')">
    </div>
    <div class="prinfo card-panel">
        <div class="prname">
            <h4 class="name">{{$user->name}}</h4>
            <div class="pos">{{$user->phone}}</div>
            {{-- <p>Design philosophies are usually for determining design goals. A design goal may range from solving the least significant individual goals.</p> --}}
        </div>
        {{-- <div class="center profile-btn">
        <a class="waves-effect waves-light btn">Follow</a>
        </div> --}}
        <div class="count">
            <h6 class="num">{{count($user->coupons)}}</h6>
            <div class="tit">Coupons</div>
        </div>
        <div class="count">
            <h6 class="num">212</h6>
            <div class="tit">Followers</div>
        </div>
        <div class="count">
            <h6 class="num">656</h6>
            <div class="tit">Following</div>
        </div>
    </div>
  </div>
