<!DOCTYPE html>
<html class="">

<head>
    <!--
         * @Package: Alix Mobile App
         * @Author: themepassion
         * @Version: 1.0
        -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <title>Alix Mobile App: Cards</title>
    <meta content="Alix Mobile App" name="description" />
    <meta content="themepassion" name="author" />


    <!-- App Icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/alix/assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/alix/assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/alix/assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/alix/assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/alix/assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/alix/assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/alix/assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/alix/assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/alix/assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/alix/assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/alix/assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/alix/assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/alix/assets/images/icons/favicon-16x16.png">
    <link rel="manifest" href="/alix/assets/images/icons/manifest.json">

    
   






    <!-- CORE CSS FRAMEWORK - START -->
    @livewireStyles
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> <!-- icons -->
        {{-- <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
        --}}
        @include('site.includes.styles')
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

<style type="text/css">
    .address_modal{
      display: none; /* Hidden by default */
      position: absolute; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 50px; /* Location of the box */
      left: 20;
      top: 1;
      width: 60%; /* Full width */
      height: 40%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: white; /* Fallback color */
      /*background-color: rgba(0,0,0,0.4);  Black w/ opacity */
      -moz-box-shadow: 0 0 5px #999;
      -webkit-box-shadow: 0 0 5px #999;
      box-shadow: 0 0 5px #999;
      opacity: 1;
    }
    .addresstextarea{

          width: 80%;
          height: 60%;
          padding: 12px 20px;
          margin: 8px 10%;
          box-sizing: border-box;
          border: 2px solid #dbdbdb;
          border-radius: 4px;
    }
    .addbtn{
        float:right;
        margin: 8px 10%;
    }
    .bg_row{
        opacity: .2;
    }
    .text-red{
        color: red;
    }
    #error_msg{
        display: none;
    }
</style>

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->


<body class=" html" data-header="light" data-footer="dark" data-header_align="center" data-menu_type="left"
    data-menu="light" data-menu_icons="on" data-footer_type="left" data-site_mode="light" data-footer_menu="show"
    data-footer_menu_style="light">
    <div class="preloader-background">
        <div class="preloader-wrapper">
            <div id="preloader"></div>
        </div>
    </div>



    <!-- START navigation -->
    @include('site.includes.navbar')
    @include('site.includes.menu')


    {{-- <div class="container">
        <div class="section">
            <h5 class="pagetitle">Cards</h5>
        </div>
    </div> --}}
    @yield('content')

    <div class="backtotop">
        <a class="btn-floating btn primary-bg">
            <i class="mdi mdi-chevron-up"></i>
        </a>
    </div>
@include('site.includes.bottom-menu-tab')
    <!-- PWA Service Worker Code -->
    @include('site.includes.scripts')



</body>

</html>
