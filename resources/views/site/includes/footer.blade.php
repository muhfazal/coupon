<footer class="page-footer center social-colored ">
    <div class="container footer-content">
        <div class="row">
            <div class="">
                <h5 class="logo">Coupon App</h5>
                {{-- <p class="text">Alix has tons of features and functionalities making it an ultimate PWA solution. --}}
                </p>
            </div>
            <div class="link-wrap">

                {{-- <ul class="social-wrap">
                    <li class="social">
                        <a class="" href="#!"><i class="mdi mdi-facebook"></i></a>
                        <a class="" href="#!"><i class="mdi mdi-twitter"></i></a>
                        <a class="" href="#!"><i class="mdi mdi-dribbble"></i></a>
                        <a class="" href="#!"><i class="mdi mdi-google-plus"></i></a>
                        <a class="" href="#!"><i class="mdi mdi-linkedin"></i></a>

                    </li>
                </ul> --}}
            </div>
        </div>
    </div>

    <div class="footer-copyright">
        <div class="container">
            &copy; Copyright <a class=""
                href="/">CouponApp</a>. All rights reserved.
        </div>
    </div>
</footer>
