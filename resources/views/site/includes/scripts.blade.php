<!-- PWA Service Worker Code -->


    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

    <!-- CORE JS FRAMEWORK - START -->
    <!-- // -->
    <script src="{{ asset('/alix/modules/jquery/jquery-2.2.4.min.js')}}"> </script> 
    <script src="{{ asset('/alix/modules/materialize/materialize.js')}}"> </script> 
    <script src="{{ asset('/alix/modules/perfect-scrollbar/perfect-scrollbar.min.js')}}"> </script>
    <script src="{{ asset('/alix/assets/js/variables.js')}}"> </script>
   <!--  <script src="{{ asset('dist/js/croppie.js')}}"></script> -->
    
   
    <!-- // -->
    <!-- <script src="/alix/modules/jquery/jquery-2.2.4.min.js"></script> -->    
    <!-- <script src="/alix/modules/materialize/materialize.js"></script>
    <script src="/alix/modules/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/alix/assets/js/variables.js"></script> -->
    <!-- <link href="{{ asset('assets/carehome/css/style.css')}}" rel="stylesheet">--> 
    <!-- CORE JS FRAMEWORK - END -->


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <script type="text/javascript">
      $(document).ready(function(){
          $(".carousel-fullscreen.carousel-slider").carousel({
            fullWidth: true,
            indicators: true,
          }).css("height", $(window).height());
          setTimeout(autoplay, 3500);
          function autoplay() {
              $(".carousel-fullscreen.carousel-slider").carousel("next");
              setTimeout(autoplay, 3500);
          }

      });   </script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE TEMPLATE JS - START -->
    <!-- <script src="/alix/modules/app/init.js"></script>
    <script src="/alix/modules/app/settings.js"></script>
    <script src="/alix/modules/app/scripts.js"></script> -->

    <script src="{{ asset('/alix/modules/app/init.js')}}"> </script>
    <script src="{{ asset('/alix/modules/app/settings.js')}}"> </script>
    <script src="{{ asset('/alix/modules/app/scripts.js')}}"> </script>

    <!-- END CORE TEMPLATE JS - END -->


    <script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
      $('.preloader-background').delay(10).fadeOut('slow');
    });
    </script>
