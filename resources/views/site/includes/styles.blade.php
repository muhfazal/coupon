  <!-- CORE CSS FRAMEWORK - START -->
  <!-- <link href="/alix/assets/css/preloader.css" type="text/css" rel="stylesheet" media="screen,projection" />  
  <link href="/alix/modules/materialize/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="/alix/modules/fonts/mdi/materialdesignicons.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="/alix/modules/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection" />
   -->
  <link href="{{ asset('alix/assets/css/preloader.css')}}" rel="stylesheet">
  <link href="{{ asset('alix/modules/materialize/materialize.min.css')}}" rel="stylesheet">
  <link href="{{ asset('alix/modules/fonts/mdi/materialdesignicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('alix/modules/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet">
  <!-- CORE CSS FRAMEWORK - END -->

  <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

  <!-- CORE CSS TEMPLATE - START -->


    <!-- <link href="/alix/assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection" id="main-style" /> -->
    <link href="{{ asset('alix/assets/css/style.css')}}" rel="stylesheet">
  <!-- CORE CSS TEMPLATE - END -->
