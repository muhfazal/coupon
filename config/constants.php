<?php
return [
    'pagination_count' => 5,
    'settings' => [
        'initial_coupons_for_shop' => 100,
        'coupons_in_set' => 100,
    ],
    'request_status' => [
        'pending' => 'PENDING',
        'fullfilled' => 'FULLFILLED',
        'rejected' => 'REJECTED',
    ],
    'aws_url' => env('AWS_URL')
];
